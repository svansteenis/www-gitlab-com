---
layout: markdown_page
title: Global Compensation
---

## On this page
{:.no_toc}

- TOC
{:toc}



## Compensation Principles
{: #compensation-principles}

1. We're an open organization and want to be transparent about our compensation principles and [paying local rates](https://about.gitlab.com/handbook/people-operations/global-compensation/#paying-local-rates), while not disclosing individual compensation. We've developed a [compensation calculator](https://about.gitlab.com/handbook/people-operations/global-compensation/#compensation-calculator) that provides an estimate of market rates for various roles, across the globe.
1. We offer [stock options](https://about.gitlab.com/handbook/stock-options/) for most positions.
1. Compensation is based on market rate for the region, your job title, and your (expected) level of performance.
1. We base compensation on current position and performance, not on what we paid you last month.
1. When your position or performance changes we'll adjust your pay as soon as possible: Note that your experience level is re-evaluated when you get a new title.
1. We don't offer ping pong tables or free lunches. We think it is more important to offer people flexibility and freedom, see the [Top 10 reasons to work for GitLab on our culture page](https://about.gitlab.com/culture/#advantages/).
1. The market rate depends on your metro region.
   1. To determine your metro region, search for "{name of your city} commute" in Google. The most frequently mentioned city (per `ctrl+F`) is taken as the metro region. If none of the cities from our Numbeo database are found in the search, then the "far from metro region" procedure applies (see next).
   1. If you live far from a metro region we base our offer upon the lowest rent index number of any metro region in your country (or state, in the case of the USA), if your country is listed.
   1. When you move [you have to inform us](/handbook/people-operations/#relocation) and we may adjust your compensation up or down. Refer to the ["move calculator"](https://about.gitlab.com/jobs/move) to see what the likely impact will be.
1. We hire across the globe but we're not location agnostic. Your timezone, the market rate in your region, and vicinity to users, customers, and partners can all be factors. For example, we may favor an applicant over another because they live in a region with a lower market rate or because we need someone in that timezone. All things being equal we will hire people in lower cost markets vs. higher cost markets.
1. As you can see from our [contracts](/handbook/contracts/), compensation is typically set at a fixed monthly rate. People on quota (account executives, account managers, and sales leadership) have variable compensation that is about 50% of their On Target Earnings (OTE). Individual contributors in the sales organization have variable compensation that is purely based on commission. Success engineers currently have a lower variable component, we're not sure how this will evolve. All other people have fixed compensation (but we do have [bonuses and incentives](/handbook/incentives)).
1. Compensation decisions around level and experience levels and for functions not in the calculator are taken by the compensation committee<a name="compensation-committee"></a>. This committee consists of the CFO, CEO, and Senior Director of People Operations. When there is no time to coordinate with the committee the CEO can take a decision and inform the committee. When the CEO is away (e.g. vacation), the two other members of the committee can take a decision and inform the committee. Whatever the decision is, the compensation committee should be cc-ed (or bcc-ed) on the final email, so that the committee members can know that the loop was closed.



## Regular Compensation
{: #regular-compensation}

1. Employees of our Dutch entity (GitLab B.V.) will get their salary wired on the
25th of every month, and can see their pay slip in their personal portal on
[HR Savvy's system](https://hr-savvy.nmbrs.nl/) towards the end of the month.
1. Employees of our Dutch entity who are based in Belgium will get their salary wired  around the last day of each month and will receive their pay slip in their personal portal on [Boekfisk's system](http://www.boekfisk.be/)
1. Employees of our Dutch entity based in India that are employed through GitLab's co-employer [Lyra](http://lyrainfo.com/) will get their salary wired around the last day of the month. Lyra will send pay slips electronically but will be switching to a HR portal very soon so that pay slips and tax declaration forms can be accessed directly.
1. Employees of our US entity (GitLab Inc.) have payroll processed semi-monthly
through TriNet, and they can access their pay slips through the [TriNet portal](https://www.hrpassport.com).
1. Employees of our UK entity (GitLab Ltd) will get their salary wired on the last day of every month, and can see their pay slip via their personal portal on [Vistra's system](https://www.vistra.com/) towards the end of the month.
1. Contractors to GitLab (either entity) should send their invoices for services rendered to ap@gitlab.com
   - For 'fixed fee' contracts, it is OK to send the invoice before the time period
   that it covers is over. For example, an invoice covering the period of March 1-31 can be sent on March 25.
   - All invoices are internally reviewed, approved, and then payment is processed.
   This is usually a fast process, but be aware that it can incur delays around vacations. In principal, payments go out once per week on Fridays.
   - An invoice template can be found as a Google sheet named "Invoice Template" (also listed on the [finance page](/handbook/finance/) )
1. To process any changes, see the directions on the [system processes page](/handbook/people-operations/system-processes/#processing-changes).



## Performance-Based (Merit) Increase Plan
{: #performance-based-increase-plan}


Cost of living adjustments (COLA) emphasize participation over accountability, generate mediocrity, and lead to the disengagement of high performers, while performance-based (merit) increases reward results and motivate continued development and higher achievements.

The following performance-based increase guidelines are intended to ensure a fair distribution of the merit budget based on each individual’s 2016 overall performance.

Increases will be effective with global pay cycles on May 1st for all eligible team members whose manager and Executive approve an increase recommendation.

### Budget


- The 2017 merit increase budget is set at 4.25% of annual base salary. Incentive Plans, OTE, and Bonus Plans are **not** included in the merit planning process.
- The merit budget is prorated based on each individual's hire date.
- The merit budget will be allocated by manager according to those who are eligible on their team.
- Executives are accountable for managing merit budget spend so as not to exceed the amount allocated for their department(s).

### Eligibility


- Team members hired prior to October 1, 2016 are eligible to participate in the 2017 merit increase plan.
- Full and Part time, Salaried, and Hourly team members are eligible.
- Individuals who received an adjustment (increase to bring them into experience range) based on the compensation calculator are eligible.

- Individuals in a temporary role are **not** eligible.
- Individuals who received a promotion or transfer to a new position with an increase on or after October 1, 2016 are **not** eligible.
- Individuals who are pending a contract transtion from contractor to employee will **not** be eligible for a merit review until all contract updates have been completed.
- Individuals who are not eligible for the 2017 merit increase plan will be eligible for the 2018 merit increase plan.
- Executives are **not** eligible for the annual merit increase program at this time.

### Proration


- Increases for team members hired on or before January 31, 2016 will not be prorated.
- Increases for team members hired on or after February 1, 2016 will be prorated as follows:

      - February = 92%
      - March = 83%
      - April = 75%
      - May = 67%
      - June = 58%
      - July = 50%
      - August = 42%
      - September = 33%

- October or after hires are **not** eligible see [Eligibility](#eligibility)
- Proration adjustments cannot be overridden by the manager.


### Merit Increase Guidelines


- Merit increase guidelines assume not everyone should receive the same increase percent.
- The 2016 [performance](/handbook/people-operations/performance-reviews/) review overall rating is the primary factor in the merit increase guideline formula.
- The following guidelines will be applied based on the performance rating received:

      - Below Expectations = 1%
      - Met Expectations = 2.75%
      - Exceeded Expectations = 4.25%
      - Greatly Exceeded Expectations = 5.75%
      - Truly Outstanding = 7.50%

- The guideline will also take into account where an individual's base salary falls in the GCC range for their level and experience factor.



### Lump Sum Award


- A Lump Sum Award is a one time payment equal to the approved merit increase amount.
- Merit increases taking a team member over +20% of the GCC median for their level and/or experience factor will be awarded in a Lump Sum amount.
       - Individuals who had already reached or reach the maximum of their level and/or experience factor GCC range during the merit increase process are considered "red circled." This status means the base salary can't be increased again until the individual's level or experience factor change. Individuals who are red circled will receive any amount (greater than $100) not applied to base salary in a Lump Sum Award.
- Lump Sum Awards will be processed for all amounts due over $100 USD.



### Manager Worksheets


 - Will be distributed to managers with direct reports.
 - Only contain each manager’s direct reports.
 - Are confidential and should **NOT** be shared or discussed with anyone outside of People Ops and each manager’s management chain.
 - Will be populated with the Merit increase percentage, based on the Performance Merit Guidelines and Growth Max limits.
 - Will be open for editing with explanation, however, managers must adhere to their allocated merit pool.
 - Will be reviewed by the division Executive together with the CFO, CEO and Sr. Director of People Ops for final approval.


### Key Deliverables 


- Self and Manager [performance reviews](/handbook/people-operations/performance-reviews/) completed for anyone who is eligible for a merit increase
- People Ops downloads Overall Performance Ratings from Lattice and uploads to Manager worksheets
- Merit increase worksheets distributed to managers
- Managers review merit increase guidelines and merit pool distribution
- Managers review performance ratings and merit distribution with their manager
- People Ops audit manager worksheets
- Executive review and approve/edit manager worksheets
- Increase details shared with Finance
- Increase details uploaded to TriNet
- Increase details passed to all international payroll providers 
- Final pre payroll processing audit (People Ops and Finance) 
- Approved increases communicated to team members
- Managers discuss 2016 performance review with team members
- Increases uploaded into Bamboo HR
- Audit post-processing in Bamboo & TriNet
- Final audits completed by Finance as May payrolls are run and contractor invoices are submitted

General questions should be sent to the People Ops email for response within one business day. Please feel free to ping Abby, Brittany, or Joan directly for more immediate attention on truly urgent issues during the performance and merit review processes.

### Communicating Performance Reviews and Merit Adjustments

Once the merit process is complete, People Operations will send an email to all managers to schedule a time to deliver 2016 performance feedback and review ratings gaps between self and manager reviews. In some cases individual performance has improved over the rating received for 2016. Managers should explain that changes in performance will be documented in the first half 2017 performance cycle coming up in July.
Email People Ops after the merit increase details and performance ratings are delivered. People Operations will stage each individual's Letter of Adjustment for Merit via HelloSign, then update BambooHR with the details.

### People Operations Cross Check Data

All manager workbooks are linked using a vlookup and importrange function in Google sheets back to an administrative workbook to stay DRY. As performance ratings, manager comments, and executive adjustment are entered into the appropriate cells, all sheets populate automatically due to the feed.
To audit the workbook, verify that all formulas are copied correctly to each cell, all formulas are functioning properly with the data set, and every team member at GitLab is on the sheet.

Once all merit adjustments have been approved, create a separate google sheet for the payroll file feeds.

- Set up a feed using the vlookup and import range from the admin workbook.
- Separate each line item by country to add multiple sheets for each entity, but keep a master of all listed to create the Merit Adjustment Letters.
- Audit the starting local annual salary against a report from the payroll provider to ensure the starting data is correct on both sides before an adjustment is processed.
- Download a csv (or excel) of each entities data set with the appropriate changes and process through the payroll provider. (All payroll providers might have a different method of processing.)
- Generate the letters of adjustment using the proper template (Base Salary change, Lump Sum Merit Award, or both) and save to the google drive.
- Once the letter is signed forward to the payroll provider (if necessary) and update BambooHR. Don't forget to remove the google doc from the drive to avoid any duplications. 

## Paying Local Rates
{: #paying-local-rates}

Different than business travel, vacation, or visiting a relative for a few weeks, relocation means that you will establish yourself in a new location outside of your current metro area. If you are ending your current residential living arrangement, spending more than six months in one location as part of an extensive period of travel and/or will have your mail delivered to an address in a different city please contact us.

As stated in the [people ops section of the handbook](/handbook/people-operations/#relocation/), you should first obtain written agreement (from your manager) when planning a relocation. It is the company's discretion to offer you a contract in your new location. At the time of the location update, we will take into consideration your new metro region when making a salary offer for continued employment.

At the onset, this practice sounds harsh when moving to a lower paid region. One might argue that it seems unfair for the organization to pay someone less for the same work in the same role, regardless of where they go. However, if you look at it from another angle for a minute and compare this practice to what most companies do, it should make more sense. For example, say you work for a company with physical locations and say they haven't accepted that remote work is as productive as coming into the office yet. If you wanted to pack up and move to a location where they did not have a physical site, you would have no alternative but to resign and seek new employment in your new location. You would find quickly that companies in the area pay at local employment market rates.

Now, let's say the company did have a site in your new location and they offered the flexibility to transfer. If they did not have a similar position open, you would have to either apply for a different open position in the same company or resign and apply externally (back to the realization that other companies will pay at local market rates). If you were lucky enough that they did have a similar role in the new location, a transfer would come with a pay rate based on the local market to ensure equity across all incumbents (people in the job) by location.

Adjusting pay according to the local market in all cases is fair to everyone.  We can't remain consistent if we make exceptions to the policy and allow someone to make greater than local market rate for the same work others in that region are doing (or will be hired to do).  We realize we might lose a few good people over this pay policy, but being fair to **all** team members is not negotiable.  It is a value we stand behind and take very seriously.


## Compensation Calculator
{: #compensation-calculator}

As a natural extension of the [Compensation Principles](#compensation-principles)
outlined above, and our commitment to transparency, sharing, efficiency, directness,
and boring solutions (amongst other [values](/handbook/values)),
we developed a Compensation Calculator that we are rolling out for those roles in
which we have the most contributors, and thus for whom the question about "what is
fair compensation" comes up most frequently.

The goals of the calculator are:

1. Calculate compensation for 200+ metro regions all over the world.
1. Based on a simple formula.
1. Based on publicly available data.
1. That is as accurate as possible given the other constraints.

As with all things at GitLab, the compensation calculator is a constant work in progress.
Please send an email to `ernst@ company domain` if/when you find a big difference between what the calculator suggests vs. what market data indicates. Please make sure to include all relevant links and data.

### The formula

Your compensation = `NYC benchmark` x `(0.25 + Rent Index + Hot Market Adjustment)` x `Level Factor` x `Experience Factor` x `Contract Type Factor`

where:

- `NYC benchmark` is typically a median employee salary for the role in New York, which we determine using various sources including [crowdsourced self-reported compensation data from Payscale](http://www.payscale.com/career-news/2006/08/salary_data_whe#comment-7148). The NYC benchmark used in each position is available in the [`jobs.yml` file](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/jobs.yml) in this [website's repo](https://gitlab.com/gitlab-com/www-gitlab-com/tree/master/).
- `Rent Index` is taken from [Numbeo](https://www.numbeo.com/cost-of-living/rankings.jsp), which expresses the ratio of cost of rent in many metro areas to the average cost of rent in New York City (i.e. Rent Index in New York = 1.00). As explained in more detail below, we add 0.25 to this regardless of location, which reflects the fact that we hire better than median performers. In point 7 of [Compensation Principles](https://about.gitlab.com/handbook/people-operations/global-compensation/#compensation-principles) it is mentioned what to do if your metro area is not listed. The full list of rent indices used is stored in the [`numbeo.yml` file](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/numbeo.yml).
- `Hot Market Adjustment` is an adjustment to recognize that "hot markets" tend to have a Rent Index that is trailing (i.e. lower than) what one would expect based on compensation rates in the area. It is currently based on the [US Census Bureau's top 15 of fastest growing cities larger than 50,000 residents](http://www.census.gov/newsroom/press-releases/2016/cb16-81.html). We found that hot markets were better represented by high growth in part of the suburbs than by growth of the (larger) city itself. We speculate that in the short term the existing housing supply in the core of the metro area is not flexible, so growth shows up in the suburbs. We add a factor of 0.1 for every suburb mentioned in the list for a given metro area. As an example, Georgetown TX and Pflugerville TX are in the top 15 because Austin TX is growing, so we add 2 x 0.1 to Austin's Rent Index. We decided on using a constant factor instead of (for example) the actual rate of growth of the city since we are trying to capture the truly top hot markets while not overrepresenting the accuracy of the approach.
- `Level Factor` is currently defined as junior (0.8), intermediate (1.0), senior (1.2), or lead (1.4)
- `Experience Factor` falls between 0.8 - 1.2
- `Contract Type Factor` distinguishes between employee or contractor, and can be a different factor in each country; see below for further explanation. The full list of contract type factors is stored in the [`contract_types.yml` file](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/contract_types.yml).

See the calculator in action for example for the Developer role, on the Developer [job description](https://about.gitlab.com/jobs/developer/).

To convert USD to local currency, we use the January 1 and July 1 [Oanda](https://www.oanda.com/currency/converter/) currency conversion rates.

### How was it developed?

In developing the compensation formula above, we looked at the compensation of our
team members which had been set in the past (without the formula), and found out
that there was a statistically significant correlation between compensation and the
factors that are now in the formula. We purposefully chose to look for correlations
with metrics that are probably causal and definitely relevant in people's lives (the rent!).
This also has the advantage of letting us work with data that is readily available
publicly, as opposed to trying to scour the web for market compensation rates for _all_
roles in _all_ locations. Perhaps surprisingly, there was a stronger correlation between
compensation and _rent index_ than with the more general _cost of living_ index available
through Numbeo (or the _cost of living with rent_ index, for that matter); and so we moved
ahead with the Rent Index.

It was a small step to go from the initial linear regression to picking the coefficients that are now in the formula, except that we 'discovered' that an offset was needed in the Rent Index to make things work (i.e. to have the formula 'predict' compensations that were in line with current actual compensations). As a consequence of this offset of 25%, for an employee in New York their median compensation will be 25% _higher_ than what the New York benchmark would have suggested. This probably reflects that we generally hire better than 'median' performers.

The contract type factor helps to make the distinction between an employee and a contractor for those countries where we offer both contract types. For example, a typical contractor may have to bear the costs of their own health insurance, social security taxes and so forth, leading to a higher compensation for the contractor. As another example the costs also vary for employees between countries, for example if there are more government mandated programs, this commonly leads to more costs for the employer and a lower pay for the individual. The contract type factor is meant to capture these differences.

#### Contract Type Factors

This list will be expanded as we gain more experience with the calculator and as we are able to offer employee contracts (as opposed to only contractor contracts) in various countries. Visit our [contracts page](https://about.gitlab.com/handbook/contracts/) to learn more about the different types of contracts we offer.

| Country | Employee | Contractor |
| --------| -------- | ---------- |
<% data.contract_types.sort_by(&:country).each do |contractType| %>
  <% country_name = contractType.country %>
  <% employee_factor = contractType.employee_factor ? sprintf('%0.2f', contractType.employee_factor).to_s : '--' %>
  <% contractor_factor = contractType.contractor_factor ? sprintf('%0.2f', contractType.contractor_factor).to_s : '--' %>
  <% if country_name == '*' %>
    <% country_name = 'Rest of World' %>
  <% end %>
  <%= "| #{country_name} | #{employee_factor} | #{contractor_factor} |" %>
<% end %>
