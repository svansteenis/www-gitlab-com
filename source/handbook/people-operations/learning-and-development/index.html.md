---
layout: markdown_page
title: "Learning and Development"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction
{: #introduction}

Welcome to the Learning & Development (L&D) page! L&D is an essential part of any organization's growth, success, and overall business strategy. We want to support the development of GitLabbers' competencies, skills and knowledge by providing the right tools and opportunities to progress their own personal and professional development.

One way to promote development is to offer courses designed to add or improve knowledge in areas such as business and financial acumen, communication, management, organization, and productivity. We chose to implement a learning management system (LMS), Grovo, to help us manage the design process and delivery of content for these and other topics of interest.

## Grovo
{:.no_toc}

### What is Grovo?

[Grovo](https://www.grovo.com/) is a Learning Management System (LMS) designed to help us create and deliver short, engaging learning tracks (courses) focused on a specific learning opportunity. Whether you're on your desktop, laptop, tablet, or phone, you can start and stop a course and pick up where you left off at any time. Grovo's content is delivered using a method called Microlearning.
There are 4 types of courses in Grovo.
1. IC, for individual contributors, content and quiz in Grovo
1. MGR, for managers, content and quiz in Grovo
1. UNI, about GitLab, content on university, optional quiz in Grovo
1. All other types, custom code based on the department (FIN, SLS, MRK, ENG, etc.), content in a url under /courses for example https://about.gitlab.com/handbook/people-operations/courses/sls-101, quiz in Grovo. 
Creating content in Grovo is not allowed because everyone should be able to contribute, all videos are on Youtube.


### What is Microlearning?

Microlearning provides short bursts of information that can be applied right away, helping people build the skills they need to be successful at their jobs. Think of these bite-size pieces of learning like sentences: each conveys just one complete thought, but when you put a few of them together, you build a more complex idea. Each microlearning moment is made of a digestible morsel of information combined with a short practice exercise. It's hyper specific, so you can get the right help, right when you need it: like when composing an email, working with new spreadsheet software, or practicing a big speech. Bit by bit, these microlearning moments add up to better job performance today and continual improvement going forward. Microlearning isn't just breaking up content into smaller pieces. It's a way to make learning digestible and effective, transforming not just what you know, but what you actually do, every day.

### Grovo at GitLab

We're a remote and globally distributed team that requires something more than the traditional learning approaches and courses that currently exist. We want to foster a new way of learning that mirrors GitLab's culture.

Take a coffee break with Grovo and see how much you can learn. It can take just under 60 seconds to sharpen your skills!

### The Quizzes

Quizzes are a tool used to test understanding and transfer of knowledge. After each micro learning lesson in Grovo there are usually two quiz questions at the end which have been designed to engage and reinforce what the learner has just heard.  You can read more about the science behind quizzes in [Creating a Learning Environment Using Quizzes](https://www.td.org/Publications/Blogs/L-and-D-Blog/2013/09/Creating-a-Learning-Environment-Using-Quizzes) by Swati Karve for the [Association for Talent Development](https://www.td.org/)
Following the initial introduction of Grovo we asked for feedback and the majority of that was about the quizzes. We have now taken them out of the individual lessons and put them right at the end of the chapter(s). This should make it a much more smoother experience. Once you click Start Training, you just sit back and learn!


### Frequently Asked Questions (FAQs)


**Q** I just got my login details for Grovo what do I do once I have logged in?

**A** Welcome to the new learning experience! Once you have logged in you will be taken directly to the _Learn_ screen where you will have some courses already pre-assigned to you. Get started with Grovo by taking the IC 001 Grovo for New Users course. This will give you a spin around the platform and help get you orientated ready for even more learning!

**Q** I have forgotten my password, how do I reset it?

**A** You can head to the [login screen](https://app.grovo.com/login), click on the _Forgotten Password?_ Link. Type in your email address and then click on _Reset Password_ button. A new password will be emailed to you. Please make sure you change the temporary password and save the new one to your 1Password.

**Q** If I receive a message to move to the next chapter, does that mean I got 100% on the quiz?

**A** No, it means you have completed and finished this chapter

**Q** What happens if I don’t achieve the required percentage to pass?

**A** Don't worry :) You can take the course again by pinging us (the people ops team). We will be able to assign a retake for you so you can have another go

 **Q** How do I retake a quiz?

**A** It is not possible to just retake the quiz by itself. You will need to take the course again which you can do by letting people ops know the course name and we can reassign it to you.

**Q** How do I review the results of the quiz I just took?

**A** If you're taking a course with no chapters, after clicking submit you will be able to immediately review your answers to the quiz you have just taken.
If the course has multiple chapters, after clicking Submit, click on the X at the top right of the message box to review your quiz responses and clarify the correct answers for any questions you missed. When you have finished reviewing the quiz results, click Next to advance to the next chapter.

**Q** Can I see which questions I got wrong or right in detail?

**A** Yes, once you have completed the entire course along with the quiz, a message will appear scroll down and you can see the questions and if you got one wrong you can click on the "details" link on the right hand side of the question to reveal the correct answer.


### Course Listings
{:.no_toc}

The course times below are estimates which also include the time to complete a quiz.


### For Individual Contributors

 - **IC 004 Social Media (28 mins)**

- In today's connected world social media is becoming an essential tool for generating business, responding to customers and sharing content. This course provides some hints, advice and guidance on how to use social media responsibly.

- **IC 120 Building Effective Communication Skills (13 mins)**

 - Communication is a blend of art and science.  To be effective communicators, we must be diligent about practicing and improving our skills.

 - **IC 130 Collaboration & Consensus (8 mins)** 

 - Collaboration and consensus are effective ways to work together as a team towards a common goal. Find out more about these approaches in this course.

- **IC 140 Productivity Under Pressure (10 mins)**

 - Explore ways to effectively manage your workload when the pressure is really on.

 - **IC 141 Effective Productivity (30 mins)** 

 - Discover how to schedule time efficiently, prioritize effectively and improve concentration so that your productively is maximized.

 - **IC 143 How to Manage Projects (36 mins)**

 - This course will give an overview of the general process of managing projects. This includes defining, scoping and identifying project tasks. The second half of the course will cover how to include others in your project plan.


### For Managers

 - **MGR 100 The Role of a Manager (15 mins)**

 - Taking the leap from individual contributor to manager is great for your career, but it will also introduce challenges you haven't dealt with before.  Learn how to effectively navigate in the role of a manager.

- **MGR 101 Develop yourself as a Manager (1hr)**

 - This course has five chapters covering; management styles, decision making approaches, data driven management, being human and professional development. This is to provide you with a fuller perspective on how you as a manager can really develop yourself and your role.

 - **MGR 120 Communicating Effectively (18 mins)**

 - See [IC Communicating Effectively](#for-individual-contributors)

 - **MGR 140 Productivity Under Pressure (12 mins)**

 - See [IC Productivity Under Pressure](#for-individual-contributors)  

 - **MGR 160 Managing Performance Issues (12 mins)**

 - Identifying and addressing performance issues early will impact positively on a team's moral, engagement and ability to achieve results while also reducing turnover.  Managers who establish and expect accountability will develop stronger individual contributors and earn the respect of their team.

 - **MGR 161 Develop Your Team (31 mins)**

 - Your team members are all different, which is part of what makes them unique. Before you get the best out them you need to first take some time and understand their working styles, strengths and weaknesses. A crucial part of a manager's role is to give feedback. In addition, you need to know when to give it and the types of approaches to feedback so that your team members respond in a positive way.

 - **MGR 162 Motivate & Enable Your Team (43 mins)**

 - Learn how to motivate your team and understand what it means to be engaged. Discover if you have a strong team commitment and what motivates your team to invest in their work.

- **MGR 165 Self Improvement & Team Dynamics (31 mins)**

 - Understanding yourself, your emotions and reactions allows you to master them so you can better support your team. A crucial part of getting the most out of your team is by letting them know you can be trusted and they can trust each-other

 - **MGR 170 Financial Fundamentals (18 mins no quiz)**

 - Learn all about generating revenue and profit. Understand the foundations of financial information and the importance of budgeting

 - **MGR 200 Strategic Management (35 mins)**

 - Big picture thinking requires planning and understanding the business at all levels of management. Leading or directing across teams requires influence and excellent communication skills. You need to establish and leverage good networks to get the results and resources you need to make everyone succeed.

 - **MGR 210 Fostering Creativity & Innovation (44 mins)**

 - Discover tools and techniques that will encourage your team to bring their great ideas forward. Ask yourself what does it mean to be creative and how can you as leader use design thinking to encourage others in your team to do the same? The final chapter of this course is all about change, be ready to adapt to change by planning and communicating your vision. Here you will also learn how to measure and evaluate the effectiveness of change initiatives.