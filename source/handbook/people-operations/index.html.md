---
layout: markdown_page
title: "People Operations"
---

## Communication
{:.no_toc}

- [**Public Issue Tracker**](https://gitlab.com/gitlab-com/peopleops/issues/); please use confidential issues for topics that should only be visible to GitLabbers at GitLab)
- You can also send an email to the People Operations group (see the "GitLab Email Forwarding" google doc for the alias), or ping an individual member of the People Operations team, as listed on our [Team page](https://about.gitlab.com/team/).
- [**Chat channel**](https://gitlab.slack.com/archives/peopleops); please use the `#peopleops` chat channel for questions that don't seem appropriate to use the issue tracker or the internal email address for.

## Other pages related to People Operations

- [Benefits](/handbook/benefits/)
- [Compensation and Title Changes](/handbook/people-operations/compensation-title-changes/)
- [Global Compensation](/handbook/people-operations/global-compensation/)
- [Hiring process](/handbook/hiring/)
- [Functional Group Updates](/handbook/people-operations/functional-group-updates)
- [Leadership](/handbook/leadership/)
- [Learning & Development](/handbook/people-operations/learning-and-development)
- [Onboarding](/handbook/general-onboarding/)
- [Offboarding](/handbook/offboarding/)
- [OKRs and LatticeHQ](/handbook/people-operations/okr/)
- [Performance Reviews](/handbook/people-operations/performance-reviews/)
- [Travel](/handbook/travel/)
- [Underperformance](/handbook/underperformance)
- [Visas](/handbook/people-operations/visas)

## On this page
{:.no_toc}

- TOC
{:toc}


## Role of People Operations
{: #role-peopleops}

In general, the People Operations team and processes are here as a service to the rest of the team; helping make your life easier so that you can focus on your work and contributions to GitLab. On that note, please don't hesitate to [reach out](https://about.gitlab.com/handbook/people-operations/#communication) with questions! In the case of a conflict between the company and a team member, People Operations works "on behalf of" the company.

## Team Directory
{: #directory}

The [team directory](https://gitlab.bamboohr.com/employees/directory.php?pin) is in BambooHR, and is accessible to all GitLabbers. This is your one-stop directory for phone numbers and addresses (in case you want to send your team mate an awesome card!).

- Please make sure that your own information stays up to date, and reach out to People Ops if you need any help in doing so.
- Please make sure that your address and phone information are written in such a way that your team mates can reach you from a different country. So, for example, include `+[country code]` in front of your phone number.

## Birthday Swag

BambooHR should send an email to People Ops the day before and the day of a team member's birthday. Celebrate by sending the team member an email (template below) and post on the #general channel on Slack so all team members can help them celebrate.

Birthday Email Template: "Happy Birthday! Please use this link [insert link] to redeem your birthday swag! You will need to login or create an account with the swag store to receive your free birthday shirt! The username will be your GitLab email. Once you are are in the store you should see your options to order a GitLab birthday shirt. Please let people ops know if you have any questions."

The link for the swag store birthday campaign is located in the People Ops vault in 1Password.

Monitor the swag store open orders to place orders that team members have entered for birthday swag.

## Letter of Employment and Reference Request Policy

If you need a letter from GitLab verifying your employment/contractor status, please send the request to People Ops citing what information is needed. We will provide most recent title, dates of employment, and salary information. We will also verify, but not provide National Identification Numbers. People Ops will send you the letter once it is completed. In addition, if the request comes from a third party, People Ops will always verify that the information is appropriate to share. If you are a US Employee you can follow these [instructions](https://drive.google.com/a/gitlab.com/file/d/0B4eFM43gu7VPYlZ4RWNpaFZHRU1UYjJJdmxXaUlFLTJ0OXhB/view?usp=sharing) for an automated verification through TriNet.

GitLabbers are not authorized by the company to speak on its behalf to complete reference requests for GitLabbers no longer working for GitLab. If a team member would like to give a personal reference based on their experience with the former team member, it must be preceded by a statement that the reference is not speaking on behalf of the company. To reinforce this fact, personal references should never be on company letterhead and telephone references should never be on company time. You do not need permission from GitLab to give a personal reference. Remember to always be truthful in reference check and try to give a majority negative reference; instead refuse to provide one. Negative references can result in legal action in some jurisdictions.

If an ex team member acted in a malicious way against GitLab we'll do a company wide announcement on the team call not to provide a reference.


## Office addresses
{: #addresses}

- For the SF office, see our [visiting](https://about.gitlab.com/visiting/) page.
- For the NL office, we use a postbox address listed in the "GitLab BV address" note in the Shared vault on 1Password. We use [addpost](https://www.addpost.nl) to scan our mail and send it along to a physical address upon request. The scans are sent via email to the email alias listed in the "GitLab Email Forwarding" google doc.
- For the UK office, there is a Ltd registered address located in the "GitLab Ltd (UK) Address" note in the Shared vault on 1Password

## Regular compensation

1. Employees of our Dutch entity (GitLab B.V.) will get their salary wired on the
25th of every month, and can see their pay slip in their personal portal on
[HR Savvy's system](https://hr-savvy.nmbrs.nl/) towards the end of the month.
1. Employees of our Dutch entity who are based in Belgium will get their salary wired  around the last day of each month and will receive their pay slip in their personal portal on [Boekfisk's system](http://www.boekfisk.be/)
1. Employees of our Dutch entity based in India that are employed through GitLab's co-employer [Lyra](http://lyrainfo.com/) will get their salary wired around the last day of the month. Lyra will send pay slips electronically but will be switching to a HR portal very soon so that pay slips and tax declaration forms can be accessed directly.
1. Employees of our US entity (GitLab Inc.) have payroll processed semi-monthly
through TriNet, and they can access their pay slips through the [TriNet portal](https://www.hrpassport.com).
1. Employees of our UK entity (GitLab Ltd) will get their salary wired on the last day of every month, and can see their pay slip via their personal portal on [Vistra's system](https://www.vistra.com/) towards the end of the month.
1. Contractors to GitLab (either entity) should send their invoices for services rendered to ap@gitlab.com
   - For 'fixed fee' contracts, it is OK to send the invoice before the time period
   that it covers is over. For example, an invoice covering the period of March 1-31 can be sent on March 25.
   - All invoices are internally reviewed, approved, and then payment is processed.
   This is usually a fast process, but be aware that it can incur delays around vacations. In principal, payments go out once per week on Fridays.
   - An invoice template can be found as a Google sheet named "Invoice Template" (also listed on the [finance page](/handbook/finance/) )
1. To process any changes, see the directions on the [system processes page](/handbook/people-operations/system-processes/#processing-changes).

## Policies

### Background checks

Beginning in May 2017, we will obtain employment and criminal background checks for existing team members based on specific project, client, and/or department assignments. Team members in Support and Success Engineering positions, People Ops, Finance, Sales (client-dependent), and the Executive team have been selected to go through this process first. Other positions/departments may be added in the future based on business requirements.

The background checks will be ordered from a selected provider and will cover criminal history for the last 7 years and employment history for the last 5 years and/or the three most recent employers. We will also require employment and criminal background checks as a condition of employment for the same positions affected by the compliance requirement.

Team members and candidates selected for this process will be required to complete an online application to initiate their background check. The application process includes signing a disclosure and a consent form which explains the rights of an individual undergoing a background examination. The application process is designed to take less than fifteen minutes to complete.

US and International team members will be required to complete the background check process. Applicants outside of the US or non-citizens will be required to provide a passport number and national ID number.

To prepare ahead for the employment application process, please gather each previous employer's name and address, your position title held, employment start and end dates, manager’s name and title, their phone number, and email address. Details for a Human Resources contact can be entered instead of a manager's contact details.

Applicants may be required to submit a form of picture ID to process their background check. For security purposes, documentation via email will not be accepted; applicants should check their email for the secure link that the background check company automatically sends in order to upload their document.

Background checks add a layer of complexity to the employment relationship because they are used to make employment decisions. Therefore, we will continue to develop this draft policy to ensure we apply a fair and consistent process which is as respectful to the privacy of our team members as possible while remaining compliant.  

### Sick time - taking and reporting
{: #sick-time}

In keeping with our [values](/handbook/values) of freedom, efficiency, transparency, kindness, and boring solutions, we have crafted the following protocol around sick leave for all GitLabbers.

**All GitLabbers**

- If you or a loved one is ill, we want you to take care of yourself or your loved one(s). To facilitate this, you should take sick leave when you need it. Sick leave is meant to be used when you are ill, or to care for family members including your parent(s), child(ren), spouse, registered domestic partner, grandparent(s), grandchild(ren), and sibling(s).
- You do need to report when you take sick leave, either by emailing your manager and People Ops, or by using the "Request time off" function in BambooHR. This way, it can be tracked in BambooHR and related payroll systems.
- If you need sick leave for more than 8 consecutive calendar days, notify your manager and People Ops to accommodate an extended leave request. What can (or must) be accommodated varies from location to location: GitLab will comply with the applicable laws in your specific location.
- Upon request, you should be able to provide proper documentation of the reason for your sick leave (doctor's note).

**Details for specific groups of GitLabbers**

- Employees of GitLab Inc. who receive a pay stub from TriNet will see sick time accrue on their pay stub at the rate of 0.0346 hrs per hour worked (3 hours of sick leave per semi-monthly pay-period) for a maximum accrual and carry-over of 72 hours per year. GitLab's policy is more generous than this, in the sense that you can take off non-accrued sick time as written above (a negative balance may show on your pay stub). Sick time does not get paid out in case of termination, nor does it reduce your final paycheck in case of a negative balance. Related to the topic of extended leave requests, see information about [short term disability](/handbook/benefits/#std-ltd) through TriNet / your state.
- Employees of GitLab B.V. have further rights and responsibilities regarding sick time based on Dutch law, as written into their employment [contracts](https://about.gitlab.com/handbook/contracts/#employee-contractor-agreements).

### Hiring Significant Other or Family Members

GitLab is committed to a policy of employment and advancement based on **qualifications and merit** and does not discriminate in favor of or in opposition to the employment of significant others or relatives. Due to the potential for perceived or actual conflicts, such as favoritism or personal conflicts from outside the work environment, which can be carried into the daily working relationship, GitLab will hire or consider other employment actions concerning significant others and/or relatives of persons currently employed or contracted only if:
a) candidates for employment will not be working directly for or supervising a significant other or relative, and
b) candidates for employment will not occupy a position in the same line of authority in which employees can initiate or participate in decisions involving a direct benefit to the significant other or relative. Such decisions include hiring, retention, transfer, promotion, wages, and leave requests.

This policy applies to all current employees and candidates for employment.

### Gift Policy

People Operations will send flowers for a birth, death, or significant event of a team member. This policy applies to an immediate relationship to the GitLab team member. Management can send a request to People Operations in order to ensure that a gift is sent in the amount of 75-125 USD.

### Relocation

If your permanent address is changing, notify People Operations of the new address within the pay cycle of the move.  The best way to do this is by logging in to BambooHR and changing your address under the **Personal** tab. This triggers a message to the BambooHR admin to review the change and "accept" it.

If you are going to spend six months or more in one location this will be considered as a relocation and your compensation will be evaluated based on the new metro region.

- If your relocation is to a different metro area, then to stay aligned with our [compensation principles](/handbook/people-operations/global-compensation-calculator/#compensation-principles) and per the [standard contract agreements](/handbook/contracts), you should obtain written agreement first (from your manager). It is the company's discretion to offer you a contract in your new location. In almost all situations the compensation will change, for an idea about the impact please see our [move calculator](https://about.gitlab.com/jobs/move).
- The People Operations Specialist will review the new location's impact on compensation. If the team member is moving to a lower cost of living, the change only needs to be approved by their manager and the Sr. Director of People Operations. If the team member is moving to a higher cost of living, the People Ops Specialist will escalate to the Sr. Director of People Ops and the CEO for approval.
- People Ops will check that any necessary changes to payroll and benefits administration are processed in time.
- People Ops will process any changes that are agreed on, and file the email in BambooHR.
- If there are any questions or concerns, please reach out to the Sr. Director of People Ops.

#### Transfer Employee to Different Location in TriNet

1. Go to HR Passport homepage.
1. Click Find.
1. Select find person by Name.
1. Type the name, click search.
1. From the choices, select the name.
1. On the left side of the screen, select Employment Data.
1. Select Employee Transfer.
1. Change location and fill in necessary information.
1. Select Update.

## Administrative details of benefits for US-based employees
{: #benefits-us}

**401k**

1. You are eligible to participate in GitLab’s 401k as of the 1st of the month after your hire date.
1. You will receive a notification on your homepage in TriNet Passport once eligible,
if you follow the prompts it will take you to the Transamerica website https://www.ta-retirement.com/
or skip logging in to TriNet Passport and go directly to https://www.ta-retirement.com/
after the 1st of the month after your hire date.
1. Once on the home page of https://www.ta-retirement.com/ go to "First Time User Register Here".
1. You will be prompted for the following information
   1. Full Name
   1. Social Security Number
   1. Date of Birth
   1. Zip Code
1. Once inside the portal you may elect your annual/pay-period contributions, and Investments.

## Using BambooHR

We use [BambooHR](https://gitlab.bamboohr.com) to keep all team member information
in one place. All team members (all contract types) are in BambooHR.
We don't have one contact person but can call BambooHR if we want any changes made in the platform. The contact info lives in the Secretarial Vault in 1Password.

Some changes or additions we make to BambooHR require action from our team members.
Before calling the whole team to action, prepare a communication to the team that is approved by the Sr. Director of People Operations.

Team Members have employee access to their profile in BambooHR and should update any data that is out-dated or incorrect. If there is a field that cannot be updated, please reach out the the People Operations Specialist with the change.

## Using RingCentral
{: #ringcentral}

Our company and office phone lines are handled via RingCentral. The login credentials
are in the Secretarial vault on 1Password. To add a number to the call handling & forwarding
rules:

- From the Admin Portal, click on the Users button (menu on left), select the user for which you
want to make changes (for door handling, pick extension 101).
- A menu appears to the right of the selected user; pick "Call Handling & Forwarding" and review
the current settings which show all the people and numbers that are alerted when the listed User's
number is dialed.
- Add the new forwarding number (along with a name for the number), and click Save.

## Mental Health Awareness

1. At GitLab we strive to create a Stigma-Free Workplace. In accordance with the National Mental Health Association and the National Council for Behavioral Health we would like to:
  * Educate employees about the signs and symptoms of mental health disorders.
  * Encourage employees to talk about stress, workload, family commitments, and other issues.
  * Communicate that mental illnesses are real, common, and treatable.
  * Discourage stigmatizing language, including hurtful labels such as “crazy,” “loony” or “nuts.”
  * Help employees transition back to work after they take leave.
  * Consult with your employee assistance program.

1. What are we doing to get there?
  * Per an open [issue](https://gitlab.com/gitlab-com/peopleops/issues/138), People Operations will be developing training for managers on this topic.
  * GitLab would also like to encourage GitLabbers to take their [time off](https://about.gitlab.com/handbook/paid-time-off) to properly take care of themselves. We encourage the team to go to yoga, take a long lunch, or anything else in their day to day life that assists in their mental and emotional well-being.
  * In addition to our current EAP programs available for employees, we encourage GitLabbers to take a look at (Working Through It)(http://www.workplacestrategiesformentalhealth.com/wti/Home.aspx) for insight into reclaiming well-being at work, off work, and return to work.
  * We believe that our values and culture lends itself to being able to discuss mental health open and honestly without being stigmatized, but let's work together to make it even more inclusive.
    * For example, Finding the right words:
      * "How can we help you do your job?"
      * "You’re not your usual self."
      * "Do you want to talk about it?"
      * "It's always OK to ask for help."
      * "It’s hard for me to understand exactly what you’re going through, but I can see that it’s distressing for you."

Any questions or concerns? Please feel free to speak with anyone in People Ops!

## Dutch work permits

Some of our GitLabbers in the Netherlands have a "werkvergunning" or work permit under the [highly skilled migrants](https://ind.nl/EN/business/employer/highly-skilled-migrants/Pages/default.aspx) category of the Immigration and Naturalization Service (IND).

- GitLab is a recognized organization ("erkend referrent") with the IND, and Savvy provides support with respect to applying for new visas / permits or extending existing ones.
- Work permits must be renewed at the end of each contract period, but at minimum once every 5 years.
- At the time of applying for permit renewal, the application must satisfy various criteria including an age-dependent [minimum salary requirement](https://www.ind.nl/en/individuals/employee/costs-income-requirements/Income-requirements) (with a step at age 30). This requirement should be taken into consideration when issuing a new contract, since the contract can be made valid for just a year or for an indefinite period; thus triggering more or less frequent re-applications for work permit extensions.

Here is a [generally helpful guide](http://www.expatica.com/nl/visas-and-permits/When-your-residence-permit-expires-or-you-want-to-leave-the-Netherlands_108416.html) on considerations around permit extensions.


## Paperwork people may need to obtain mortgage in the Netherlands
{: #dutch-mortgage}

When your employment contract is for a fixed period of time (e.g. 12 months) you'll need a "werkgeversverklaring".
This document describes your salary and states that your employer expects to continue to employ
you after the contract expires (assuming the performance of the employee doesn't degrade).
This document has to be filled in by hand, preferably using blue ink, and must be signed
and stamped. If there is no stamp (as is the case for GitLab) an extra letter (also signed)
must be supplied that states the employer has no stamp. While the language of these
documents doesn't matter, the use of Dutch is preferred.

Employees also have to provide a copy of a payslip that clearly states not only their
monthly salary but also their annual salary. These numbers must match the numbers on
the "werkgeversverklaring" down to the decimals. Mortgage providers may also require
you to provide information about your financial status, usually in the form of a report/screenshot
of your total financial status (including any savings you have). The requirements for
this seem to vary a bit between mortgage providers.

## Terminations

### Voluntary Terminations

A voluntary termination occurs when a team member informs his or her manager of a resignation.

If you are a current team member and you are thinking about resigning from GitLab, we encourage you to speak with your manager, People Ops, or another trusted team member to discuss your reasons for wanting to leave. At GitLab we want to ensure that all issues team members are facing are discussed and resolved to result in a great work environment.

If resignation is the only solution after you have discussed your concerns, then please follow these procedures.

#### Procedures

1. Team members are requested to provide an agreed upon notice of their intention to separate from the company to allow a reasonable amount of time to transfer ongoing workloads.
1. The team member should provide a written resignation letter or notification to his or her manager.
1. Upon receipt of the resignation, the manager will notify People Ops by sending a copy of the resignation letter.
1. After People Operations is made aware of the upcoming resignation an acknowledgement letter confirming last day and holiday due may be required. This will depend on the location and employment status of the team member. Examples of these can be found here:
 - [GitLab Ltd (UK) Resignation Acknowledgement](https://docs.google.com/document/d/1zV1qnZmjQaNZ3QUjrLOtod-FbboNCPmYdqCLIAc7aos/edit)
  - [GitLab BV (Netherlands) Resignation Acknowledgement](https://docs.google.com/document/d/1-9hCL2Xs5po4lZ19L2vrcmGxQIYRD-Emyci3F63kt0c/edit)
 1. After the People Ops Generalist or Director has reviewed the circumstances of the resignation, the team member may take the opportunity before the end of their notice period to share their decision to leave during a team call.
1. People Ops will send an email with the following information to the team member:
  * [Process](https://about.gitlab.com/handbook/offboarding/#returning-property) for returning all company property.
  * A review of the team member's post-termination benefits status, if applicable.
  * An exit interview. The exit interview provides team members with the opportunity to freely express views about working at GitLab. People Operations will reach out to the team member to conduct an interview via Google Hangouts. If for some reason that is not possible, People Operations will send a form to the team member to complete. Once the interview has been completed with the team member, People Operations will conduct an interview with the manager.  All comments from the exit interviews will be kept confidential. People Operations will compile data from exit interviews to determine if feedback to the head of the employee’s department or other members of management is necessary.

### Involuntary Terminations

Involuntary termination of any team member is never easy. We've created some
guidelines and information to make this process as humane as we can. Beyond the
points outlined below, make sure to refer to our guidelines on [underperformance](/handbook/underperformance),
as well as the [offboarding checklist](https://gitlab.com/gitlab-com/peopleops/blob/master/.gitlab/issue_templates/offboarding.md).

When an employee is absent from work for six consecutive workdays, there is no
entry on the availability calendar for time off, and fails to contact his or
her supervisor, they can be terminated for job abandonment.

#### Overall process

Ideally, the manager and the team member have walked through the guidelines on [underperformance](/handbook/underperformance) before reaching this point.

1. Manager: reach out to People Operations for assistance. People Ops will ask about what the performance issues have been, how they have been attempted to be addressed, and will then prepare for the termination.
1. Manager and People Ops: discuss best mode of communicating the bad news to the team member. This discussion can happen via a private chat-channel, but it is best to be done via a video hangout. Set up a private chat-channel in any case, since this is also useful to have during the eventual call with the affected team member.
1. Manager and People Ops: Decide who will handle which part of the conversation, and if desired, practice it. It is strongly advised to have someone from People Ops on the call when the bad news is delivered.
1. Manager and People Ops: Decide what offboarding actions need to be taken _before_ the call (e.g. revoke admin permissions), or _during_ the call (e.g. revoke Slack and Gmail access), and which ones can wait until later (see the [offboarding checklist](https://gitlab.com/gitlab-com/peopleops/blob/master/.gitlab/issue_templates/offboarding.md)). Make sure someone with the necessary admin privileges is on hand (in the private chat-channel) to assist with those sensitive offboarding steps that should occur relatively quickly. Do not create the offboarding issue until _after_ the call, since even confidential issues are still visible to anyone in the team.
1. Manager: Set up a call with the team member in question. Make a separate private calendar event to invite the People Ops representative.
1. On the call: deliver the bad news up-front, do not beat around the bush and prolong the inevitable pain for everyone involved. A sample leading sentence can be "Thanks for joining the call ___ . Unfortunately, the reason I wanted to speak with you, is because we have decided that we have to let you go and end your employment / contract with GitLab." At this point, hand over the call to People Ops to continue. People Ops explains what led to this decision and points to the process that was followed to reach this decision. People Ops to make it clear that the decision is final, but also to genuinely listen to their side of the story since there may be useful lessons in what they say for the rest of the team e.g. regarding hiring and vetting practices.
1. People Ops: Make sure to communicate the [practical points](#offboarding-points) from the termination memo outlined below.
1. People Ops: Create the [offboarding checklist issue](https://gitlab.com/gitlab-com/peopleops/blob/master/.gitlab/issue_templates/offboarding.md), and go from there.


#### Points to cover during the offboarding call, with sample wording
{: #offboarding-points}

The following points need to be covered for any team member:

1. Final Pay: "your final check (or invoice period) is for the pay period of X and includes X days of pay”.
1. Company Property: “please return all property as explained in the handbook, also please delete GitLab’s email connection from your phone”.
1. Business Expenses: “please create your final expense report to Expensify (for employees), OR, please file any outstanding expenses with your final invoice (for contractors), so these can be reimbursed to you in a timely manner”.
1. Confidentiality and Non-Disclosure: “we know you are a professional, please keep in mind the agreement you signed when you were hired”.

The following points need to be covered for US-based employees:

1. COBRA: “your benefits will cease on last day of the month you are eligible for Consolidated Omnibus Budget Reconciliation Act (“COBRA”), the carrier (TriNet) has been notified and the carrier will send out the paperwork to your home address on file”.
1. PPACA: "You may also be eligible under the Patient Protection and Affordable Care Act (“PPACA”) for subsidized health care options via the marketplace. If you are interested it is important that you sign up with the market place well before the 15th of the month to have coverage for the following month”.
1. HIPAA: " under the Health Insurance Portability and Accountability Act of 1996 (HIPAA), if you need a certificate of credible coverage please download it from your current carrier's online portal or request it from TriNet”.
1. Unemployment insurance: "it is up to your state's labor agency (in CA: EDD) to decide if you are eligible for unemployment insurance”.
1. Please remember to keep TriNet informed if you move I want to be sure your W-2 gets to you at the end of the year. You may also contact X at GitLab (provide phone number and email address) with any other questions that you may have" (consider inviting them to contact you at anytime for any reason)

### Sample termination memo

If appropriate (to be determined by conversation with the manager, CEO, and people ops), use the following [termination memo](https://docs.google.com/document/d/11Uk8p4VJrLnDD5IAtbTwswPUUEnmeEOazS1kJMhOu70/edit?usp=sharing), which is provided here as an openly viewable Google Doc, but of course needs to be personalized and tailored to each individual's situation. As written, it is applicable to US-based employees only.

### Departures are unpleasant

As explained briefly in the [offboarding checklist](https://gitlab.com/gitlab-com/peopleops/blob/master/.gitlab/issue_templates/offboarding.md),
we do not provide any context around why people are leaving when they do. Instead we
say: "As of today, X is no longer with GitLab. Out of respect for their privacy I
can't go into details. If you have questions about tasks or projects that need to
be picked up, please let me know."

If someone is let go involuntarily, this generally cannot be shared since it affects
the individual's privacy and job performance is intentionally kept [between an
individual and their manager](/handbook/general-guidelines/#not-public).
If someone leaves voluntarily, there can be times when this is fine to share or
even for them to announce their own departure, however, there may be other times when that
isn't possible. But we don't share in either case to prevent
"silence" around a departure from becoming synonymous with saying that it was involuntary.

Such silence is unpleasant. It's unpleasant because we are human, which means
that we are generally curious and genuinely interested in the well-being of
our team members.

Is it _more_ unpleasant in a remote setting? Probably not. When people are all in
the same office building, they can "kinda sorta" know what may be coming because
of the grapevine, the water cooler, and so on. When the news hits it might be
less of a shock - only
because of unprofessional behavior in the first place. But at larger companies with
multiple office buildings, departures will tend to come as more of a surprise and
with less context (at least to the people in other buildings).  
