---
layout: markdown_page
title: "Paid time off at GitLab"
---

1. Don't frown on people taking time off, but rather encourage that people take care of themselves and others.
1. Working hours are flexible, you are invited to the [team call](#team-call) if you are available, and if you want you can post to the #working-on chat channel what is on your mind so others can offer suggestions.
1. You don't need to worry about taking time off to go to the gym, [take a nap](https://m.signalvnoise.com/sleep-deprivation-is-not-a-badge-of-honor-f24fbff47a75), go grocery shopping, doing household chores, helping someone, taking care of a loved one, etc. If something comes up or takes longer than expected and you have urgent tasks and you're able to communicate, just ensure the rest of the team **knows** and someone can pick up any urgent tasks.
1. GitLab encourages team members to volunteer within their community to take care of others.
1. Encourage your co-workers to take time out when you become aware that they are working long hours over a sustained period.
1. We have an no ask time off policy. This means that:
    * You do not need to ask permission to take time off unless you want to have more than 25 consecutive calendar days off. The 25 days limit for no ask is per vacation, not per year. You can do multiple no ask vacations per year that are more than 25 days in total, there is no limit to this. What we care about is that you get results, not how long you work. You are also welcome to take more than 25 days off in one stretch, just make sure to discuss with your manager upfront.
    * Always make sure that your job responsibilities are covered while you are away.
    * We strongly recommended to take at least a minimum of 2 weeks of vacation per year, if you take less your manager might follow up with you to discuss what is wrong.
    * Please mark your leave on the GitLab availability calendar so team members are aware of your leave in advance.  This is located under "Other Calendars -> GitLab availability".
1. You do need to ensure that not more than **half** of the people that can help with availability emergencies (the on-call heroes), regular support, sales, or development are gone at any moment. You can check for this on the availability calendar, so be sure to add time off early.
1. Please see the [On-Call](https://about.gitlab.com/handbook/on-call/)
page for information on how to handle scheduled leave for someone from the [on-call](#on-call) team.
1. Add an **appointment** to the GitLab availability calendar as you know your plans, you can always change it later. When you leave, you can also add your [status in Slack](https://about.gitlab.com/handbook/tools-and-tips/#slack-status). 
1. In case it can be useful add your planned time off as a **FYI** on the next agenda of the team call.
1. We will **help** clients during official days off, unless they are official days off in both the Netherlands and the U.S. We try to have people working who are in a country that don't have an official day off. If you need to work during an official day off in your country, you should take a day off in return.
1. If you have to respond to an incident while being on-call outside of your regular working hours, you should feel free to take off some time the following day to recover and be well-rested. If you feel pressure to _not_ take the time off to rest, refer to this part of the handbook and explain that you had to handle an incident.

### Recognizing Burnout

It is important for us to take a step back to recognize and acknowlege the feelings of being "burned out." We are not as effective or efficient when we work long hours, miss meals or forego nurturing our personal lives
for sustained periods of time. If you feel yourself or notice someone on your team experiencing [burnout](http://www.mayoclinic.org/healthy-lifestyle/adult-health/in-depth/burnout/art-20046642), be sure to address it right away. To get ahead of a problem, be sure to communicate with your manager if the following statement ever applies to you, "I'm feeling like I might burn out if the situation doesn't change."

If you, your peers or your direct reports are showing signs of burnout, you should take time out to focus on things that relax you and improve your overall health and welfare. As a manager, address burnout right away by discussing options with your team member to evaluate the workload and manage contributing stressors. 

Other [tips to avoid burnout](http://www.mayoclinic.org/healthy-lifestyle/adult-health/in-depth/burnout/art-20046642?pg=2) include:
    * Assess and pursue your interests, skills and passions
    * Take breaks during the day to eat healthy foods
    * Make time each day to increase blood and oxygen circulation which improves brain activity and functionality
    * Get plenty of restfull sleep
    
Don't let burnout creep up on you. Working remotely can allow us to create bad habits, such as working straight through lunch to get something finished. Once in a while this feels good to check that nagging task or big project off the list but don't let this behavior become a bad habit. Before long, you'll begin to feel the effects on your body and see it in your work. Take care not to burn yourself out!